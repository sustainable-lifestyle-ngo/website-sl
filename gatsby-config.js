module.exports = {
  siteMetadata: {
    title: `Sustainable Lifestyle starter`,
    description: `A custom Gatsby starter template to start a multilanguage website.`,
    author: `Gabriel Moncea`,
    twitter: `@gabimoncha`,
    email: `gabriel.moncea@sustainablelifestyle.ro`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Sustainable Lifestyle Association`,
        short_name: `Sustainable Lifestyle`,
        icon: `src/images/logo.png`,
        start_url: `/`,
        background_color: `#fcfcfc`,
        theme_color: `#fcfcfc`,
        display: `standalone`,
      },
    },
    `gatsby-plugin-offline`,
    `gatsby-plugin-styled-components`,
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: `<replace_with_GA>`,
        head: true,
        anonymize: true,
      },
    },
    {
      resolve: `gatsby-plugin-intl`,
      options: {
        path: `${__dirname}/src/intl`,
        languages: [`ro`, `en`],
        defaultLanguage: `ro`,
        // redirect: true,
      },
    },
    `gatsby-plugin-sitemap`,
    `gatsby-plugin-robots-txt`,
  ],
};
