import RobotoCondensed400 from 'fonts/RobotoCondensed400.ttf';
import RobotoCondensed700 from 'fonts/RobotoCondensed700.ttf';

export { RobotoCondensed400, RobotoCondensed700 };
