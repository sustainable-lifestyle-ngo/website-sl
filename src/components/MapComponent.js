import React, { useEffect, useRef } from 'react';
import mapboxgl from 'mapbox-gl';
import 'mapbox-gl/dist/mapbox-gl.css';
import styled from 'styled-components';
import { layout } from 'styled-system';
import { useStaticQuery, graphql } from 'gatsby';

const Wrapper = styled.div`
  ${layout}
  position: relative;
  flex: 1 0 auto;
`;

const center = [26.09, 44.44];
const zoom = 9.3;
const minZoom = 0;
const maxZoom = 22;

const MapComponent = ({ width, height, sources = {}, layers = [] }) => {
  const {
    site: {
      siteMetadata: { mapboxToken, mapStyle },
    },
  } = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            mapboxToken,
            mapStyle
          }
        }
      }
    `
  );

  if (!mapboxToken) {
    console.error(
      'ERROR: Mapbox token is required in gatsby-config.js siteMetadata'
    );
  }

  // // if there is no window, we cannot render this component
  // if (!hasWindow) {
  //   return null;
  // }

  // this ref holds the map DOM node so that we can pass it into Mapbox GL
  const mapNode = useRef(null);
  const mapRef = useRef(null);

  // construct the map within an effect that has no dependencies
  // this allows us to construct it only once at the time the
  // component is constructed.
  useEffect(() => {
    // Token must be set before constructing map
    mapboxgl.accessToken = mapboxToken;

    const map = new mapboxgl.Map({
      container: mapNode.current,
      style: mapStyle,
      center,
      zoom,
      minZoom,
      maxZoom,
    });
    mapRef.current = map;
    window.map = map; // for easier debugging and querying via console

    map.on('load', () => {
      console.log('map onload');

      Object.entries(sources).forEach(([id, source]) => {
        map.addSource(id, source);
      });

      // add layers
      layers.forEach(layer => {
        map.addLayer(layer);
      });
    });

    // hook up map events here, such as click, mouseenter, mouseleave
    // e.g., map.on('click', (e) => {})

    // when this component is destroyed, remove the map
    return () => {
      map.remove();
    };
  }, []);

  return (
    <Wrapper width={width} height={height}>
      <div ref={mapNode} style={{ width: '100%', height: '100%' }} />
    </Wrapper>
  );
};

export default MapComponent;
