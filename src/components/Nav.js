import React, { memo } from 'react';
import styled from 'styled-components';
import { position, layout, space } from 'styled-system';
import { injectIntl, Link } from 'gatsby-plugin-intl';

const NavIcon = styled.label`
  ${layout}
  height: 70%;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 10;
  transform: rotate(0deg);
  transition: 0.25s ease-in-out;
  cursor: pointer;
  margin-top: 10px;
  margin-left: 3vw;

  @media (max-width: 52em) {
    margin-left: 6vw;
  }
`;

const NavIconLine = styled.span`
  display: block;
  height: 4px;
  position: absolute;
  width: 100%;
  background: #3B845B;
  border-radius: 20px;
  left: 0;
  transform: rotate(0deg);
  transition: 0.15s ease-in-out;
`;

const NavIconLine1 = styled(NavIconLine)`
  top: 10px;
`;

const NavIconLine2 = styled(NavIconLine)`
  ${position}
`;

const NavIconLine3 = styled(NavIconLine)`
  ${position}
`;

const NavIconLine4 = styled(NavIconLine)`
  ${position}
`;

const Input = styled.input`
  display: none;

  :checked + ${NavIcon} {
    ${NavIconLine1} {
      width: 0%;
      left: 50%;
    }

    ${NavIconLine2} {
      transform: rotate(45deg);
    }

    ${NavIconLine3} {
      transform: rotate(-45deg);
    }

    ${NavIconLine4} {
      width: 0%;
      left: 50%;
    }
  }
`;

const NavModal = styled.nav`
  display: none;
  ${position}
  ${layout}
  
  ${Input}:checked ~ & {
    display: block;
    position: absolute;
    box-shadow: 2px 2px 4px 2px rgba(0,0,0,0.3);
    padding: 90px 20px 20px 0;
    top: 0;
    left: 1.5vw;
    background-color: white;

    @media (max-width: 52em) {
      left: 0;
    }
  }
`;

const RouteLinkContainer = styled.div`
  display: block;
  ${space}
  font-size: 22px;
  font-weight: bold;
  width: 100%;

  :hover {
    text-decoration: underline;
  }
`;

const RouteLinkActive = {
  textDecoration: 'underline',
};

const AnchorLinkContainer = styled.a`
  display: block;
  ${space}
  font-size: 22px;
  font-weight: bold;
  width: 100%;

  :hover {
    text-decoration: underline;
  }

  :active {
    text-decoration: underline;
  }
`;

const iconWidth = ['30px', '34px'];
const iconMiddleLineTop = ['18px', '20px'];
const iconBottomLineTop = ['26px', '30px'];
const routePadding = ['6vw', '6vw', '1.5vw'];

const RouteComponent = ({ intl, url, idx }) => (
  <Link to={`/${url}`} activeStyle={RouteLinkActive}>
    <RouteLinkContainer px={routePadding}>
      {intl.formatMessage({ id: `nav_link${idx + 1}` })}
    </RouteLinkContainer>
  </Link>
);

const AnchorComponent = ({ intl, href, idx }) => (
  <AnchorLinkContainer px={routePadding} href={href}>
    {intl.formatMessage({ id: `anchor_link${idx + 1}` })}
  </AnchorLinkContainer>
);

const RouteLink = injectIntl(RouteComponent);
const AnchorLink = injectIntl(AnchorComponent);

const Nav = ({ urls }) => {
  const renderLinks = () =>
    urls.map((item, idx) => {
      if (item.type && item.type === 'anchor') {
        const href = `#${item.url}`;
        return <AnchorLink key={href + idx} href={href} idx={idx} />;
      } else {
        return <RouteLink key={item + idx} url={item} idx={idx} />;
      }
    });

  return (
    <>
      <Input type="checkbox" id="menu" />
      <NavIcon htmlFor="menu" width={iconWidth}>
        <NavIconLine1 />
        <NavIconLine2 top={iconMiddleLineTop} />
        <NavIconLine3 top={iconMiddleLineTop} />
        <NavIconLine4 top={iconBottomLineTop} />
      </NavIcon>
      <NavModal id="nav" left="0" minWidth="200px">
        {renderLinks()}
      </NavModal>
    </>
  );
};

export default memo(Nav);
