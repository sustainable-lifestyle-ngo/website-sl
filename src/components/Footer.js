import React from 'react';
import styled from 'styled-components';
import { injectIntl } from 'gatsby-plugin-intl';

const Container = styled.footer`
  text-align: center;
  font-size: 16px;
  padding: 40px 0 20px;

  @media (max-width: 52em) {
    font-size: 16px;
  }
`;

const Link = styled.a`
  display: block;
  :hover {
    text-decoration: underline;
  }
`;

const Space = styled.div`
  height: 10px;
`;

const Footer = ({ intl }) => {
  return (
    <Container>
      <div>
        2015 - {new Date().getFullYear()} ©{' '}
        {intl.formatMessage({ id: 'title' })}
      </div>
      <div>{intl.formatMessage({ id: 'rights' })}</div>
      <Space />
      <Link
        href="https://linkedin.com/in/gabriel-moncea/"
        target="_blank"
        rel="design"
        referrerpolicy="no-referrer"
      >
        {intl.formatMessage({ id: 'design' })}
      </Link>
      <Link
        href="https://linkedin.com/in/gabriel-moncea/"
        target="_blank"
        rel="author"
        referrerpolicy="no-referrer"
      >
        {intl.formatMessage({ id: 'author' })}
      </Link>
    </Container>
  );
};

export default injectIntl(Footer);
