import styled from 'styled-components';
import { flexbox, typography } from 'styled-system';

const Paragraph = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  ${flexbox}
  ${typography}
`;

export default Paragraph;
