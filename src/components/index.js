export { default as Footer } from 'components/Footer';
export { default as GlobalStyles } from 'components/GlobalStyles';
export { default as Header } from 'components/Header';
export { default as Layout } from 'components/Layout';
export { default as Nav } from 'components/Nav';
export { default as SEO } from 'components/SEO';
export { default as Paragraph } from 'components/Paragraph';
export { default as MapComponent } from 'components/MapComponent';
