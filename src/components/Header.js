import React from 'react';
import styled from 'styled-components';
import { layout, typography } from 'styled-system';
import Img from 'gatsby-image';
import { useStaticQuery, graphql } from 'gatsby';
import { Nav } from 'components';

const Container = styled.header`
  display: flex;
  flex-direction: column;
  position: sticky;
  top: 0;
  background-color: white;
  ${layout}
`;

const Bar = styled.div`
  display: flex;
  flex-direction: row;
  ${layout}
`;

const Logo = styled.div`
  position: absolute;
  top: 16px;
  left: 0;
  right: 0;
  margin: 0 auto;
  ${layout}
`;

const Title = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-end;
  margin: 0 3vw;
  ${layout}

  @media (max-width: 52em) {
    margin: 0 6vw;
  }
`;

const Text = styled.div`
  display: flex;
  flex-direction: column;
  font-weight: bold;
  ${typography}
`;

const Aloe = styled.div`
  ${layout}
  align-items: flex-end;
  margin-bottom: 20px;
`;

const urls = [
  { url: 'home', type: 'anchor' },
  { url: 'about', type: 'anchor' },
  { url: 'project', type: 'anchor' },
];

const logoWidth = ['60px', '80px', '100px'];
const aloeWidth = ['0', '30vw', '40vw', '400px'];
const aloeDisplay = ['none', 'initial'];

const headerHeight = ['200px', '210px'];
const barHeight = ['90px', '90px', '60px', '30px'];
const titleHeight = ['90px', '90px'];
const titleFontSize = ['28px', '32px', '40px', '40px'];

const Header = () => {
  const data = useStaticQuery(graphql`
    query {
      logo: file(relativePath: { eq: "logo.png" }) {
        childImageSharp {
          fluid(maxWidth: 300) {
            ...GatsbyImageSharpFluid_withWebp_noBase64
          }
        }
      }
      aloe: file(relativePath: { eq: "aloe.png" }) {
        childImageSharp {
          fluid(maxWidth: 700) {
            ...GatsbyImageSharpFluid_withWebp_noBase64
          }
        }
      }
    }
  `);

  return (
    <Container minHeight={headerHeight}>
      <Bar minHeight={barHeight}>
        <Nav urls={urls} />
        <Logo width={logoWidth}>
          <Img
            fluid={data.logo.childImageSharp.fluid}
            alt="Gatsby Docs are awesome"
          />
        </Logo>
      </Bar>
      <Title minHeight={titleHeight}>
        <Text fontSize={titleFontSize}>
          <span>Mediul si clima.</span>
          <span>Cu subiect si predicat.</span>
        </Text>
        <Aloe width={aloeWidth} display={aloeDisplay}>
          <Img
            fluid={data.aloe.childImageSharp.fluid}
            alt="Gatsby Docs are awesome"
          />
        </Aloe>
      </Title>
    </Container>
  );
};

export default Header;
