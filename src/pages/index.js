import React from 'react';
import { injectIntl } from 'gatsby-plugin-intl';

import { Layout, SEO, Paragraph, MapComponent } from 'components';

const Home = ({ intl }) => {
  return (
    <Layout>
      <SEO title={intl.formatMessage({ id: 'home_title' })} />
      <Paragraph fontSize={['22px']} id="first">
        <h2>{intl.formatMessage({ id: 'home_title1' })}</h2>
        {intl.formatMessage({ id: 'home_text1' })}
      </Paragraph>
      <Paragraph fontSize={['22px']} id="about">
        <h2>{intl.formatMessage({ id: 'home_title1' })}</h2>
        {intl.formatMessage({ id: 'home_text1' })}
      </Paragraph>
      <Paragraph fontSize={['22px']} id="project">
        <h2>{intl.formatMessage({ id: 'home_title1' })}</h2>
        {intl.formatMessage({ id: 'home_text1' })}
      </Paragraph>
      <MapComponent width="300px" height="300px"/>
    </Layout>
  );
};

export default injectIntl(Home);
