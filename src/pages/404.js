import React from 'react';
import styled from 'styled-components';
import { injectIntl, Link } from 'gatsby-plugin-intl';

import { Layout, SEO, Paragraph } from 'components';

const Route = styled.div`
  text-align: center;
  :hover {
    text-decoration: underline;
  }
`;

const NotFoundPage = ({ intl }) => {
  return (
    <Layout
      display="flex"
      flexDirection="column"
      alignContent="center"
      justifyContent="center"
      fontSize={['22px', '26px']}
    >
      <SEO title={intl.formatMessage({ id: 'notfound_title' })} />
      <Paragraph flexDirection="row" justifyContent="center">
        {intl.formatMessage({ id: 'notfound_text' })}&nbsp;
        <span role="img" aria-label="duh">
          😓
        </span>
      </Paragraph>
      <Link>
        <Route>
          {intl.formatMessage({ id: 'notfound_link' })}&nbsp;
          <span role="img" aria-label="duh">
            🖱️
          </span>
        </Route>
      </Link>
    </Layout>
  );
};

export default injectIntl(NotFoundPage);
